# =============================================================================
# Makefile variables
# =============================================================================
VERSION=1.1
TAG=v$(VERSION)
IMAGE_NAME=ocp-cicd-tool
IMAGE_REPO=quay.io/corbsmartin/$(IMAGE_NAME)
HELM_VERSION=3.7.1
OC_VERSION=4.10.11
JQ_VERSION=jq-1.6

image:
	@podman build --no-cache -f ./Dockerfile -t $(IMAGE_NAME):$(TAG) \
		--build-arg=OC_VERSION=$(OC_VERSION) \
		--build-arg=HELM_VERSION=$(HELM_VERSION) \
		--build-arg=JQ_VERSION=$(JQ_VERSION) .
	@podman tag $(IMAGE_NAME):$(TAG) $(IMAGE_NAME):latest
	@podman tag $(IMAGE_NAME):latest $(IMAGE_REPO):latest

push: image
	@podman push $(IMAGE_REPO):latest

verify:
	@podman run --rm -it ocp-cicd-tool:v1.1 \
		echo "helm: `helm version --short`" && \
	  	echo "oc: `oc version --client -o yaml`" && \
	  	echo "jq: `jq --version`"
